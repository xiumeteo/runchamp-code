package models;

import java.util.Date;

case class Event(

	name: String,
	date: Date,
	hour: Date,
	description: String

)

